## @file IMU.py
#  This module contains a IMU class that will be used in future ME 405 projects. 
#
#  The IMU class consists of several functions: init, mode, calstatus, 
#  euler, and angval. mode function updates the BNO055 operation/power mode based on user input:
#  calstatus fetches the calibration status of the gyro, accelerometer, magnetometer, and total system; 
#  euler retrieves the euler angles of the BNO055 in a tuple (x,y,z), and angvaal retrieves the angular velocities
#  of the BNO055 in a tuple format (x,y,z).
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 19, 2020
#
#  @package encoder
#  This module contains a IMU class that will be used in future ME 405 projects. 
#
#  The IMU class consists of several functions: init, mode, calstatus, 
#  euler, and angval. mode function updates the BNO055 operation/power mode based on user input:
#  calstatus fetches the calibration status of the gyro, accelerometer, magnetometer, and total system; 
#  euler retrieves the euler angles of the BNO055 in a tuple (x,y,z), and angvaal retrieves the angular velocities
#  of the BNO055 in a tuple format (x,y,z).
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 19, 2020

import pyb

## An IMU driver object
#
#  The IMU class consists of several functions: init, mode, calstatus, 
#  euler, and angval. mode function updates the BNO055 operation/power mode based on user input:
#  calstatus fetches the calibration status of the gyro, accelerometer, magnetometer, and total system; 
#  euler retrieves the euler angles of the BNO055 in a tuple (x,y,z), and angvaal retrieves the angular velocities
#  of the BNO055 in a tuple format (x,y,z).
#
#  @author Scott Mangin
#  @copyright 2020
#  @date May 19, 2020
class IMUdriver:
    ''' This class implements a closed loop proportional controller for the
    ME405 board. '''

    def __init__ (self, I2C_ADDR):
        ''' Creates an IMU driver for the BNO055 system.
        - @param I2C_ADDR An integer specifying the address of the IMU'''
        
        self.I2C_ADDR = I2C_ADDR
        self.PWR_MODE = 0x3E      # Define constant representing PWR_MODE of IMU
        self.OPR_MODE = 0x3D      # Define constant representing OPR_MODE of IMU
        self.i2c = pyb.I2C(1, pyb.I2C.MASTER)  # create and init the IMU as a Master

    def mode (self, mode):
        ''' Updates the mode of the motor (power, setting IMU mode)
        - @param mode An integer specifying which mode is being selected by the user.'''
       
        # Constants
        PWR_MODE = self.PWR_MODE
        OPR_MODE = self.OPR_MODE
        I2C_ADDR = self.I2C_ADDR
        
        # Enabling/Disablimg IMU
        if mode == 1:
            self.i2c.mem_write(0x00, self.I2C_ADDR,  PWR_MODE) #Set Power Mode to Normal
            print('Setting to Normal Power Mode')
        elif mode == 0:
            self.i2c.mem_write(0x02, I2C_ADDR,  PWR_MODE) #Set Power Mode to Suspend
            print('Setting to Suspend Mode')
       
       # Setting IMU Mode
        if mode == 2:
            self.i2c.mem_write(0x01, I2C_ADDR,  OPR_MODE) #Set Op Mode to ACCONLY
            print('Set Op Mode to ACCONLY')
        elif mode == 3:
            self.i2c.mem_write(0x02, I2C_ADDR,  OPR_MODE) #Set Op Mode to MAGONLY
            print('Set Op Mode to MAGONLY')
        elif mode == 4:
            self.i2c.mem_write(0x03, I2C_ADDR,  OPR_MODE) #Set Op Mode to GYROONLY
            print('Set Op Mode to GYROONLY')
        elif mode == 5:
            self.i2c.mem_write(0x04, I2C_ADDR,  OPR_MODE) #Set Op Mode to ACCMAG
            print('Set Op Mode to ACCMAG')
        elif mode == 6:
            self.i2c.mem_write(0x05, I2C_ADDR,  OPR_MODE) #Set Op Mode to ACCGYRO
            print('Set Op Mode to ACCGYRO')
        elif mode == 7:
            self.i2c.mem_write(0x06, I2C_ADDR,  OPR_MODE) #Set Op Mode to MAGGYRO
            print('Set Op Mode to MAGGYRO')
        elif mode == 8:
            self.i2c.mem_write(0x07, I2C_ADDR,  OPR_MODE) #Set Op Mode to AMG
            print('Set Op Mode to AMG')
        elif mode == 9:
            self.i2c.mem_write(0x08, I2C_ADDR,  OPR_MODE) #Set Op Mode to IMU
            print('Set Op Mode to IMU')
        elif mode == 10:
            self.i2c.mem_write(0x09, I2C_ADDR,  OPR_MODE) #Set Op Mode to COMPASS
            print('Set Op Mode to COMPASS')
        elif mode == 11:
            self.i2c.mem_write(0x0A, I2C_ADDR,  OPR_MODE) #Set Op Mode to M4G
            print('Set Op Mode to M4G')
        elif mode == 12:
            self.i2c.mem_write(0x0B, I2C_ADDR,  OPR_MODE) #Set Op Mode to NDOF_FMC_OFF
            print('Set Op Mode to NDOF_FMC_OFF')
        elif mode == 13:
            self.i2c.mem_write(0x0C, I2C_ADDR,  OPR_MODE) #Set Op Mode to NDOF
            print('Set Op Mode to NDOF')
        
        
    def calstatus (self):
        ''' Updates user on calibration status'''
        # Define Constants needed for function
        I2C_ADDR = self.I2C_ADDR
        CALIB_STAT = 0x35 # Define constant representing PWR_MODE of IMU
        
        calstat = self.i2c.mem_read(1, I2C_ADDR, CALIB_STAT)
        
        # Code adapted from Manjeet_04, (Unknown), Python | Decimal to binary list conversion
        # Source Code: https://www.geeksforgeeks.org/python-decimal-to-binary-list-conversion/
        res = [int(i) for i in list('{0:0b}'.format(calstat[0]))]
        
        while len(res) < 8: #To get 8 length list, add zeros for beginning list items
            res.insert(0,0) #Inserts zeros to make list of 8 length
            
        #Checks calibration status on each subsystem
        if (res[0] == 1 & res[1] == 1): #Checks bits 6 and 7
            print('System Calibrated')
        else:
            print('System Not Calibrated')
            
        if (res[2] == 1 & res[3] == 1): #Checks bits 4 and 5
            print('Gyro Calibrated')
        else:
            print('Gyro Not Calibrated')
            
        if (res[4] == 1 & res[5] == 1): #Checks bits 2 and 3
            print('Accelerometer Calibrated')
        else:
            print('Accelerometer Not Calibrated')
            
        if (res[6] == 1 & res[7] == 1): #Checks bits 0 and 1
            print('Magnetometer Calibrated')
        else:
            print('Magnetometer Not Calibrated')
        
    def euler (self):
        '''Updates Euler Angle Orientation for user'''  
        import ustruct
        
        #Constants
        I2C_ADDR = self.I2C_ADDR
        EUL_Heading_LSB = 0x1A
        
        #Reads the dataset for euler angle data
        data = self.i2c.mem_read(6, I2C_ADDR, EUL_Heading_LSB)
        
        #"Unpacks" each dataset of LSB and MSB into one signed integer
        values = ustruct.unpack('<hhh',data)
        
        # Code adapted from Milor123, (Unknown), How to divide each element in a tuple by a single integer? [closed]
        # Source Code: https://stackoverflow.com/questions/36386627/how-to-divide-each-element-in-a-tuple-by-a-single-integer
        euler = [x/16.0 for x in values] #Divides each integer by LSB tick value from datasheet
        return euler
    
    def angvel (self):
        '''Updates Angular Velocity Orientation for user'''
        import ustruct
        
        #Constants
        I2C_ADDR = self.I2C_ADDR
        GYR_DATA_X_LSB = 0x14
        
        #Reads the dataset for gyroscopic data (angular velocity)
        angdata = self.i2c.mem_read(6, I2C_ADDR, GYR_DATA_X_LSB)
        
        #"Unpacks" each dataset of LSB and MSB into one signed integer
        angval = ustruct.unpack('<hhh',angdata)
        
        # Code adapted from Milor123, (Unknown), How to divide each element in a tuple by a single integer? [closed]
        # Source Code: https://stackoverflow.com/questions/36386627/how-to-divide-each-element-in-a-tuple-by-a-single-integer
        angular = [x/16.0 for x in angval] #Divides each integer by LSB tick value from datasheet
        return angular
    
## @file main.py
#  Main program used for testing the IMU driver functionality.
#
#  Main program used for testing the IMU driver functionality.
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 19, 2020
#
#
         
# Import desired modules
import pyb
import utime
import ustruct
#import IMU

#Constants Section:
I2C_ADDR        = 0x28 # Define constant representing device address of IMU

imu = IMUdriver(I2C_ADDR)

# Turns on the IMU
imu.mode(1)

# Changes OP_MODE to NDOF
imu.mode(13)

#While loop to
i = 0
while i < 10:
    imu.calstatus()
    i = i + 1
    utime.sleep(1)
    
i = 0
while i < 50:
    eulerang = imu.euler()
    angv = imu.angvel()
    print('Euler Angles: {:}'. format(eulerang))
    print('Angular Velocities: {:}'. format(angv))
    i = i + 1
    utime.sleep(1)

imu.mode(0)
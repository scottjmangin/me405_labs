## @file main.py
#  Main program used for testing the IMU driver functionality.
#
#  Main program used for testing the IMU driver functionality.
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 19, 2020
#
#
         
# Import desired modules
import pyb
import utime
import ustruct
import IMU

#Constants Section:
I2C_ADDR        = 0x28 # Define constant representing device address of IMU

imu = IMU.IMUdriver(I2C_ADDR)

# Turns on the IMU
imu.mode(1)

# Changes OP_MODE to NDOF
imu.mode(13)

#While loop to
i = 0
while i < 10:
    imu.calstatus()
    i = i + 1
    utime.sleep(1)
    
i = 0
while i < 50:
    eulerang = imu.euler()
    angv = imu.angvel()
    print('Euler Angles: {:}'. format(eulerang))
    print('Angular Velocities: {:}'. format(angv))
    i = i + 1
    utime.sleep(1)

imu.mode(0)
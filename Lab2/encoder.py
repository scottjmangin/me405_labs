## @file encoder.py
#  This module contains a encoder class that will be used in future ME 405 projects. 
#
#  The encoder class consists of several functions: init, update, get_position, 
#  set_position, and get_delta. Update function takes the current encoder readout
#  and updates the absolute position of the encoder with measures for over/underflow.
#  get_position fetches the absolute position of the motor, set_position sets the 
#  absolute position of the motor, and get_delta gets the difference in timer ticks
#  between the previous two updates.
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 5, 2020
#
#  @package encoder
#  This module contains a encoder class that will be used in future ME 405 projects. 
#
#  The encoder class consists of several functions: init, update, get_position, 
#  set_position, and get_delta. Update function takes the current encoder readout
#  and updates the absolute position of the encoder with measures for over/underflow.
#  get_position fetches the absolute position of the motor, set_position sets the 
#  absolute position of the motor, and get_delta gets the difference in timer ticks
#  between the previous two updates.
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 5, 2020

import pyb

## An encoder driver object
#
#  The encoder class consists of several functions: init, update, get_position, 
#  set_position, and get_delta. Update function takes the current encoder readout
#  and updates the absolute position of the encoder with measures for over/underflow.
#  get_position fetches the absolute position of the motor, set_position sets the 
#  absolute position of the motor, and get_delta gets the difference in timer ticks
#  between the previous two updates.
#
#  @author Scott Mangin
#  @copyright 2020
#  @date May 5, 2020
class EncoderDriver:
    ## This class implements an encoder driver for the
    # ME405 board.

    def __init__ (self, pin_1, pin_2, tim):
        '''Creates a encoder driver. Initializing encoder timers (setting timer channels,
        setting the timer to encoder counting mode) and initializing variables
        for overall encoder position, previous encoder position, and the delta between
        the previous positon and the current position as read by tim.counter.'''
        
        ##
        # @param pin_1 A pyb.Pin object to use as the motor encoder A phase.
        # @param pin_2 A pyb.Pin object to use as the motor encoder B phase.
        # @param tim A pyb.Timer object to use for encoder timing with pre-defined
        # prescalar and period values.
        # @param encpos An internal variable object to use as the current encoder position.
        # @param prevpos An internal variable object to use to store the previous encoder position.
        # @param delta An internal variable object to use to store the difference between the current
        # and the previous encoder position.
        # @param timer An internal variable object to use as a single encoder position fetch to make
        # sure computing time does not affect motor position.'''
        
        print ('Initializing Encoder')
        
        self.tim = tim #Sets the timer parameters to internal variable
        
        #Resets the parameters for each encoder object
        self.encpos = 0 #Encoder position variable
        self.prevpos = 0 #Previous encoder position variable
        self.delta = 0 #Variable for difference between current and previous position

        #Sets channel for timer, using pin locations set by pin_1 and pin_2 per encoder.
        #Also sets the type of timer to be encoder
        self.tch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin=pin_1)
        self.tch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin=pin_2)

    def update (self):
        '''Updates the encoder position based on the input from the encoder channel.
        Adapts current position of motor to absolute position and handles over 
        and underflow of encoder readout. Can work in both directions, where clockwise
        direction turn of the motor (facing shaft) adds a positive position, and a
        counterclockwise rotation adds a negative position. Also handles delta and
        prevpos for other definition.'''
        
        self.timer = self.tim.counter() #sets timer variable to count readout of encoder
        
        self.delta = self.timer - self.prevpos #gets delta based on prevpos and timer
        
        #This if-else statement is for handling over/underflow as well as small delta values
        if abs(self.delta) > 30000: #Approximately half the number line of 16 bit
            if self.delta > 0: # Equation if going from low 16-bit number to high 16-bit number (CCW)
                self.encpos = self.encpos - (self.prevpos + (65535-self.timer))
            elif self.delta < 0: #Equation if going from high 16-bit number to low 16-bit number (CCW)
                self.encpos = self.encpos + (self.timer + (65535-self.prevpos))
        else:
            self.encpos = self.encpos + self.delta #Equation for small delta (no under/overflow)
        
        #Sets previous timer for
        self.prevpos = self.timer

    def get_position (self):
        '''Returns the current encoder position of desired motor.'''
        return self.encpos
        
    def set_position (self, setpos):
        '''Sets the encoder position value of desired motor.'''
        ##
        # @param setpos A variable object to use as an input of the desired motor position.
        print ('Setting Position to {:}'. format(setpos))
        self.encpos = setpos

    def get_delta (self):
        '''Reports the delta value calculated in the update function.'''
        return self.delta
"""
@mainpage Encoder Driver Module 

@details This module contains an encoder class that will be used in future ME 405 projects. 

<a href="https://bitbucket.org/scottjmangin/me405_labs/src/master/Lab1/">Repository Link</a> 

Created on Mon May 5 20:23:50 2020.

@author Scott Mangin

@file lab2.py

"""

class EncoderDriver:
    ''' This class implements an encoder driver for the
    ME405 board. '''

    def __init__ (self, pin_1, pin_2, tim):
        ''' Creates a encoder driver by initializing encoder timers (setting
        timer channels, setting the timer to encoder counting mode) and initializing variables
        for overall encoder position, previous encoder position, and the delta between
        the previous positon and the current position as read by tim.counter.
        @param pin_1 A pyb.Pin object to use as the motor encoder A phase.
        @param pin_2 A pyb.Pin object to use as the motor encoder B phase.
        @param tim A pyb.Timer object to use for encoder timing with pre-defined
        prescalar and period values.
        @param encpos An internal variable object to use as the current encoder position.
        @param prevpos An internal variable object to use to store the previous encoder position.
        @param delta An internal variable object to use to store the difference between the current
        and the previous encoder position.
        @param timer An internal variable object to use as a single encoder position fetch to make
        sure computing time does not affect motor position.'''
        
        print ('Initializing Encoder')
        
        self.tim = tim #Sets the timer parameters to internal variable
        
        #Resets the parameters for each encoder object
        self.encpos = 0 #Encoder position variable
        self.prevpos = 0 #Previous encoder position variable
        self.delta = 0 #Variable for difference between current and previous position

        #Sets channel for timer, using pin locations set by pin_1 and pin_2 per encoder.
        #Also sets the type of timer to be encoder
        self.tch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin=pin_1)
        self.tch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin=pin_2)

    def update (self):
        ''' Updates the encoder position based on the input from the encoder channel.
        Adapts current position of motor to absolute position and handles over 
        and underflow of encoder readout. Can work in both directions, where clockwise
        direction turn of the motor (facing shaft) adds a positive position, and a
        counterclockwise rotation adds a negative position. Also handles delta and
        prevpos for other definition.'''
        
        self.timer = self.tim.counter() #sets timer variable to count readout of encoder
        
        self.delta = self.timer - self.prevpos #gets delta based on prevpos and timer
        
        #This if-else statement is for handling over/underflow as well as small delta values
        if abs(self.delta) > 30000: #Approximately half the number line of 16 bit
            if self.delta > 0: # Equation if going from low 16-bit number to high 16-bit number (CCW)
                self.encpos = self.encpos - (self.prevpos + (65535-self.timer))
            elif self.delta < 0: #Equation if going from high 16-bit number to low 16-bit number (CCW)
                self.encpos = self.encpos + (self.timer + (65535-self.prevpos))
        else:
            self.encpos = self.encpos + self.delta #Equation fro small delta (no under/overflow)
        
        #Sets previous timer for
        self.prevpos = self.timer

    def get_position (self):
        '''Returns the current encoder position of desired motor.'''
        return self.encpos
        
    def set_position (self, setpos):
        '''Sets the encoder position value of desired motor.'''
        print ('Setting Position to {:}'. format(setpos))
        self.encpos = setpos

    def get_delta (self):
        '''Reports the delta value calculated in the update function.'''
        return self.delta
        
if __name__ == '__main__':
    
    # Import desired modules
    import pyb
    import time
    
    # First Motor Encoder Configuration
    pin_PB6 = pyb.Pin (pyb.Pin.cpu.B6); # Sets CPU Pin based on desired encoder input
    pin_PB7 = pyb.Pin (pyb.Pin.cpu.B7); # Sets CPU Pin based on desired encoder input
    tim4 = pyb.Timer(4, period=65535, prescaler=0) #Sets timer based on desired parameters
    
    enc1 = EncoderDriver(pin_PB6, pin_PB7, tim4) # Sets first encoder to have motor pins and timer config

    # Second Motor Encoder Configuration
    pin_PC6 = pyb.Pin (pyb.Pin.cpu.C6); # Sets CPU Pin based on desired encoder input
    pin_PC7 = pyb.Pin (pyb.Pin.cpu.C7); # Sets CPU Pin based on desired encoder input
    tim8 = pyb.Timer(8, period=65535, prescaler=0) #Sets timer based on desired parameters

    enc2 = EncoderDriver(pin_PC6, pin_PC7, tim8) # Sets second encoder to have motor pins and timer config

    # For Testing:

    loop = 0 # Sets the loop to 0 for while loop update
    
    while loop < 10:
        enc1.update() # Updates encoder value
        print ('Position1 is {:}'. format(enc1.get_position())) # Reports position from update
        loop = loop + 1 # Increments loop
        time.sleep(1) # Wait for 1 second
    
    loop = 0
    
    while loop < 10:
        enc2.update() # Updates encoder value
        print ('Position2 is {:}'. format(enc2.get_position())) # Reports position from update
        loop = loop + 1 # Increments loop
        time.sleep(1) # Wait for 1 second
    
    # Report final positions to show values stored separately
    print ('Position1 is {:}'. format(enc1.get_position()))
    print ('Position2 is {:}'. format(enc2.get_position()))
    
    # Report previous position delta to show values stored separately
    print ('Delta1 is {:}'. format(enc1.get_delta()))
    print ('Delta2 is {:}'. format(enc2.get_delta()))
    
    # Sets position to 0 for each encoder to show functionality
    enc1.set_position(0)
    enc2.set_position(0)
     
    # Shows set_pos functonality
    print ('Position1 is {:}'. format(enc1.get_position()))
    print ('Position2 is {:}'. format(enc2.get_position()))
    

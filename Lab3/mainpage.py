## @file mainpage.py
#  Functions as the main page of the bitbucket website.
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is a website dedicated to Scott Mangin's ME405 code repo. This site
#  contains classes that will be used in this course.
#
#  @section sec_mot Motor Driver
#  The purpose of this driver is to control any Nucleo-Driver compatible motor.
#  The code used to create the object is: motor.MotorDriver(EN_pin, IN1_pin, IN2_pin, tim).
#  Testing motor driver included CCW and CW operation, as well as speed control using duty cycles.
#  Please see the motor class which is part of the \ref motor package.
#  <a href="https://bitbucket.org/scottjmangin/me405_labs/src/master/Lab1/">Repository Link</a>  
#
#  @section sec_enc Encoder Driver
#  The purpose of the encoder driver is to operate the encoder module on the Nucleo-Driver motors.
#  The code used to create the object is: encoder.EncoderDriver(pin_1, pin_2, timer). Testing
#  was done to verify no position overflow/underflow on absolute position, as well as getting position,
#  getting delta, and setting absolute position. Please see the encoder class which is part of the \ref encoder package.
#  <a href="https://bitbucket.org/scottjmangin/me405_labs/src/master/Lab2/">Repository Link</a> 
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 5, 2020
#

## @file motor.py
#  This module contains a motor class that will be used in future ME 405 projects. 
#
#  The motor class consists of several functions: init, enable, disable, and set duty.
#  Enable/Disable are for operating the motor, set duty is for setting the duty cycle
#  of power delivered to the motor, setting the speed.
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 5, 2020
#
#  @package motor
#  This module contains a motor class that will be used in future ME 405 projects. 
#
#  The motor class consists of several functions: init, enable, disable, and set duty.
#  Enable/Disable are for operating the motor, set duty is for setting the duty cycle
#  of power delivered to the motor, setting the speed.
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 5, 2020

import pyb

## A motor driver object
#
#  The motor class consists of several functions: init, enable, disable, and set duty.
#  Enable/Disable are for operating the motor, set duty is for setting the duty cycle
#  of power delivered to the motor, setting the speed.
#  @author Scott Mangin
#  @copyright 2020
#  @date May 5, 2020
class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''

    def __init__ (self, EN_pin, IN1_pin, IN2_pin, tim):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin
        and IN2_pin. '''
        
        self.EN = EN_pin
        self.IN1 = IN1_pin
        self.IN2 = IN2_pin
        self.timer = tim
        
        self.EN.low () #Sets the enable pin to low voltage to stop the motor
        print ('Creating a motor driver')

    def enable (self):
        ''' Enables the function of the motor.'''

        print ('Enabling Motor')
        
        self.EN.high () #Sets the enable pin to high voltage to start the motor

    def disable (self):
        ''' Disables the function of the motor.'''
        print ('Disabling Motor')
        self.EN.low () #Sets the enable pin to low voltage to stop the motor

    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in the clockwise direction (facing shaft), 
        negative values cause effort in the counter-clockwise direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        
        # Initializing the timer channels for IN1 and IN2 pins
        self.tch1 = self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1)
        self.tch2 = self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2)
        
        if duty >= 0: #If a positive duty cycle, turn clockwise at specified duty value
            self.tch1.pulse_width_percent(duty)
            self.tch2.pulse_width_percent(0)
        elif duty < 0: #If a negative duty cycle, convert to positive and turn CCW at specified duty value
            duty = -duty
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(duty)

        '''The use of duty cycle set to 0 instead of IN#.low () is to ensure that
        motor can turn both directions without interference from .low command'''

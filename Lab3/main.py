## @file main.py
#  Main program used for testing the closed loop Kp control.
#
#  Main program used for testing the closed loop Kp control.
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 13, 2020
#
#
         
# Import desired modules
import pyb
import utime
import motor
import closedloopKp
import encoder

pin_PB6 = pyb.Pin (pyb.Pin.cpu.B6);
pin_PB7 = pyb.Pin (pyb.Pin.cpu.B7);
tim4 = pyb.Timer(4, period=65535, prescaler=0)

enc = encoder.EncoderDriver(pin_PB6, pin_PB7, tim4)

# Create the pin objects used for interfacing with the motor driver
pin_EN = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4);
pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5);

# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq = 20000);

# Create a motor object passing in the pins and timer
moe = motor.MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)

# Closed Loop Driver:
clcon = closedloopKp.CLKpControl()

# For Testing:
repeat = 0

moe.enable()

#Reset speed
moe.set_duty(0)

'''Main function of the code that prompts for the user to enter a valid desired index
location for the Fibonnaci sequence and returns the number at that location. Has user
interface that only allows for correct index numbers and user prompts for error messages.'''       
repeat = 1

while repeat == 1: #Using variable repeat to allow for both multiple runs and program end
    while True:
        idx = input('Specify Kp Value (Positive Integer): ') #Gets user input for Kp value and saves string to idx
        if  idx.isdigit(): #Checks for if inputted value is numeric only
            desiredkp = int(idx) #Turns inputted string into an integer
            break
        else:
            print('Invalid input, please try again')
    
    # Initialization for each test
    loop = 0
    poslist = []
    timelist = []
    enc.set_position(0)
    desiredpos = 5000 #In Ticks
    
    while loop < 200: #Updates encoder and motor position, uses that information to get the duty cycle, and sets the duty cycle accordingly.
        enc.update()
        motorpos = enc.get_position()
        kpduty = clcon.update(desiredkp,motorpos,desiredpos)
        moe.set_duty(kpduty)
        poslist.append(motorpos) #Position list
        timelist.append(10*loop) #Time list
        loop = loop + 1
        utime.sleep_ms(10)
    
    moe.set_duty(0) #Stops motor
    print(poslist) #Prints motor position list
    print(timelist) #Prints time list
    
    while True:
        prompt = input('Would you like to try another Kp (Y/N)?: ')
        if prompt == 'Y' or prompt == 'y': #If yes prompt, breaks this while loop and restarts main function
            break
        elif prompt == 'N' or prompt == 'n': #Otherwise, sets repeat to != 1 to break main loop and breaks this while loop
            repeat = 0
            moe.disable()
            print('Program ending.')
            break
        else:
            print ('Invalid Input') #Any other input than y or n is invalid
    

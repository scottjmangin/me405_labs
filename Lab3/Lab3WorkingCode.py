"""
Test code for motor (works)

"""

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''

    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin
        and IN2_pin. '''
        pin_EN.low () #Sets the enable pin to low voltage to stop the motor
        print ('Creating a motor driver')

    def enable (self):
        ''' Enables the function of the motor.'''

        print ('Enabling Motor')
        
        pin_EN.high () #Sets the enable pin to high voltage to start the motor

    def disable (self):
        ''' Disables the function of the motor.'''
        print ('Disabling Motor')
        pin_EN.low () #Sets the enable pin to low voltage to stop the motor

    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in the clockwise direction (facing shaft), 
        negative values cause effort in the counter-clockwise direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        
        # Initializing the timer channels for IN1 and IN2 pins
        t3ch1 = tim.channel(1, pyb.Timer.PWM, pin=pin_IN1)
        t3ch2 = tim.channel(2, pyb.Timer.PWM, pin=pin_IN2)
        
        if duty >= 0: #If a positive duty cycle, turn clockwise at specified duty value
            t3ch1.pulse_width_percent(duty)
            t3ch2.pulse_width_percent(0)
        elif duty < 0: #If a negative duty cycle, convert to positive and turn CCW at specified duty value
            duty = -duty
            t3ch1.pulse_width_percent(0)
            t3ch2.pulse_width_percent(duty)

        '''The use of duty cycle set to 0 instead of IN#.low () is to ensure that
        motor can turn both directions without interference from .low command'''

class EncoderDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''

    def __init__ (self, pin_1, pin_2, tim):
        ''' Creates a encoder driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin
        and IN2_pin. '''
        print ('Initializing Encoder')
        
        self.tim = tim
        
        self.encpos = 0
        self.prevpos = 0
        
        self.delta = 0

        self.tch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin=pin_1)
        self.tch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin=pin_2)

    def update (self):
        
        self.timer = self.tim.counter()
        
        self.delta = self.timer - self.prevpos
        
        if abs(self.delta) > 30000: #Approximately half the number line of 16 bit
            if self.delta > 0:
                self.encpos = self.encpos - (self.prevpos + (65535-self.timer))
            elif self.delta < 0:
                self.encpos = self.encpos + (self.timer + (65535-self.prevpos))
        else:
            self.encpos = self.encpos + self.delta
        
        self.prevpos = self.timer

    def get_position (self):
        return self.encpos
        
    def set_position (self, setpos):
        print ('Setting Encoder Position to {:}'. format(setpos))
        self.encpos = setpos

    def get_delta (self):
        return self.delta
    
class CLKpControl:
    ''' This class implements a closed loop proportional controller for the
    ME405 board. '''

    def __init__ (self):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
         '''

    def update (self, SpecKp, motpos):
        ''' Updates the motor PWM value'''
        setpoint = 5000
        err = motpos-setpoint
        Kp = (err/65535)*(100)*-SpecKp
        if abs(Kp) > 100:
            if Kp > 0:
                Kp = 100
            elif Kp < 0:
                Kp = -100
        return Kp
        
if __name__ == '__main__':
    
    # Import desired modules
    import pyb
    import utime
    
    pin_PB6 = pyb.Pin (pyb.Pin.cpu.B6);
    pin_PB7 = pyb.Pin (pyb.Pin.cpu.B7);
    tim4 = pyb.Timer(4, period=65535, prescaler=0)
    
    enc = EncoderDriver(pin_PB6, pin_PB7, tim4)
    
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
    pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4);
    pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5);

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq = 20000);

    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)

    # Closed Loop Driver:
    clcon = CLKpControl()
    
    # For Testing:
    repeat = 0
    
    moe.enable()
    
    #Reset speed
    moe.set_duty(0)
    
    '''Main function of the code that prompts for the user to enter a valid desired index
    location for the Fibonnaci sequence and returns the number at that location. Has user
    interface that only allows for correct index numbers and user prompts for error messages.'''       
    repeat = 1
    
    while repeat == 1: #Using variable repeat to allow for both multiple runs and program end
        while True:
            idx = input('Specify Kp Value (Positive Integer): ') #Gets user input for Kp value and saves string to idx
            if  idx.isdigit(): #Checks for if inputted value is numeric only
                desiredkp = int(idx) #Turns inputted string into an integer
                break
            else:
                print('Invalid input, please try again')
        
        # Initialization for each test
        loop = 0
        poslist = []
        timelist = []
        enc.set_position(0)
        
        while loop < 200:
            enc.update()
            motorpos = enc.get_position()
            kpduty = clcon.update(desiredkp,motorpos)
            moe.set_duty(kpduty)
            poslist.append(motorpos)
            timelist.append(10*loop)
            loop = loop + 1
            utime.sleep_ms(10)
        
        moe.set_duty(0)
        print(poslist)
        print(timelist)
        
        while True:
            prompt = input('Would you like to try another Kp (Y/N)?: ')
            if prompt == 'Y' or prompt == 'y': #If yes prompt, breaks this while loop and restarts main function
                break
            elif prompt == 'N' or prompt == 'n': #Otherwise, sets repeat to != 1 to break main loop and breaks this while loop
                repeat = 0
                moe.disable()
                print('Program ending.')
                break
            else:
                print ('Invalid Input') #Any other input than y or n is invalid
    
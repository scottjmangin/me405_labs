# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 13:05:26 2020
@file Lab0.py
@author: sjman
"""
def fib(idx):
    ''' This function calculates the Fibonacci number coresponding 
    to the specified valid index location of the user.
    
    - @param idx: An integer specifying the index location of the desired
                 Fibonnaci number.
    
    -Adapted from: Modi, Saket, (Unknown), Fibonacci Recursion (Method 1), [Source Code]. 
    https://www.geeksforgeeks.org/python-program-for-program-for-fibonacci-numbers-2/'''
    
    if  idx < 0: #if it is less than zero, it is an invalid location
        print('Invalid Index Location')
    elif idx == 0:
        return 0
    elif idx == 1: #If index location is 1, return 0
        return 1
    else:
        return fib(idx-1)+fib(idx-2) #Use recursion formula provided in directions to calculate fibonnaci number
        
if __name__ == '__main__': 
    '''Main function of the code that prompts for the user to enter a valid desired index
    location for the Fibonnaci sequence and returns the number at that location. Has user
    interface that only allows for correct index numbers and user prompts for error messages.'''       
    repeat = 1
    while repeat == 1: #Using variable repeat to allow for both multiple runs and program end
        while True:
            idx = input('Specify index integer for desired Fibonacci number: ') #Gets user input for index address and saves string to idx
            if  idx.isdigit(): #Checks for if inputted value is numeric only
                idx = int(idx) #Turns inputted string into an integer
                break
            else:
                print('Invalid input, please try again')
        
        print ('Calculating Fibonacci number at index n = {:}.'. format(idx)) #Prints calculation message for user with their specified index address
        num = fib(idx) #Runs Fibonacci function and returns with Fibonnaci number at index location
        print('The Fibonacci number is: {:}'. format(num)) #Displays Fibonnaci number for user
        
        while True:
            prompt = input('Would you like to try another index location (Y/N)?: ')
            if prompt == 'Y' or prompt == 'y': #If yes prompt, breaks this while loop and restarts main function
                break
            elif prompt == 'N' or prompt == 'n': #Otherwise, sets repeat to != 1 to break main loop and breaks this while loop
                repeat = 0
                print('Program ending.')
                break
            else:
                print ('Invalid Input') #Any other input than y or n is invalid    
else:
    print('Naming Error')   # In case something is wrong with name block

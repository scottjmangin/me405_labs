"""
@mainpage Motor Driver Module 

@details This module contains a motor class that will be used in future ME 405 projects. 

<a href="https://bitbucket.org/scottjmangin/me405_labs/src/master/Lab1/">Repository Link</a> 

Created on Wed Apr 22 20:23:50 2020.

@author Scott Mangin

@file lab1.py


"""

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''

    def __init__ (self, EN_pin, IN1_pin, IN2_pin, tim):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on IN1_pin
        and IN2_pin. '''
        
        self.EN = EN_pin
        self.IN1 = IN1_pin
        self.IN2 = IN2_pin
        self.timer = tim
        
        self.EN.low () #Sets the enable pin to low voltage to stop the motor
        print ('Creating a motor driver')

    def enable (self):
        ''' Enables the function of the motor.'''

        print ('Enabling Motor')
        
        self.EN.high () #Sets the enable pin to high voltage to start the motor

    def disable (self):
        ''' Disables the function of the motor.'''
        print ('Disabling Motor')
        self.EN.low () #Sets the enable pin to low voltage to stop the motor

    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in the clockwise direction (facing shaft), 
        negative values cause effort in the counter-clockwise direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        
        # Initializing the timer channels for IN1 and IN2 pins
        self.tch1 = self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1)
        self.tch2 = self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2)
        
        if duty >= 0: #If a positive duty cycle, turn clockwise at specified duty value
            self.tch1.pulse_width_percent(duty)
            self.tch2.pulse_width_percent(0)
        elif duty < 0: #If a negative duty cycle, convert to positive and turn CCW at specified duty value
            duty = -duty
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(duty)

        '''The use of duty cycle set to 0 instead of IN#.low () is to ensure that
        motor can turn both directions without interference from .low command'''

if __name__ == '__main__':
    
    # Import desired modules
    import pyb
    import time
    
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
    pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4);
    pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5);

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq = 20000);

    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)

    # For Testing:
    
    # Enable the motor driver
    moe.enable()

    # Set the duty cycle to 10 percent
    moe.set_duty(20)
    
    # Wait for 3 seconds
    time.sleep(3)
    
    # Set the duty cycle to 10 percent
    moe.set_duty(-20)
    
    # Wait for 3 seconds
    time.sleep(3)
    
    # Disable the motor driver
    moe.disable()


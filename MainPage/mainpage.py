## @file mainpage.py
#  Functions as the main page of the bitbucket website.
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is a website dedicated to Scott Mangin's ME405 final project and code repo.
#
#  @section p_final Project FF Report
#  My term project for ME 405 was to construct a 2-DOF (degree of freedom) robot arm that would control the height and orientation of an ultrasonic sensor using an IMU (inertial measurement unit) sensor, where the ultrasonic sensor takes measurements of the height of various objects. This initial project proposal met all the requirements listed in the project proposal scope. However, due to a malfunctioning second motor actuator, I was not able to complete the scope of the original project. Therefore, I was forced to modify the scope of my project to accommodate for the broken motor.
# 
#  Here is the video demonstration of my final project: <a href="https://youtu.be/XB8XPmWbItY">Project Demonstration</a> 
# 
#  The robot uses two motors for actuation, three sensors (encoders, IMU, ultrasonic), and popsicle stick linkage to raise and lower the ultrasonic sensor so as to measure the heights of different objects accurate to plus-minus one centimeter. As shown in the project video above, the ultrasonic sensor was able to measure its height from the table (albeit rounding down to 8 cm), and was able to measure the height of the small sensor box (which was around 2.6 cm), the height measured by the ultrasonic sensor changing to 5 cm. Next, moving the IMU up and down changed the desired motor position (in 1440.6 ticks per 360 degrees) of the base motor and was able to raise and lower the robotic arm. The second motor was supposed to counter the rotation of the base motor to have the ultrasonic sensor constantly focused on the ground for measuring height. As shown in the video, the weight of the arm put a significant strain on the base motor, where the motion of the arm is jerky when raising the arm but smoother when lowering the arm. If I were to continue this project into the future, I would incorporate a gear system for increased torque as well as a revision to the ratio of motor ticks to output angle for that reduced speed system.
# 
#  Due to damaging my second motor, I could not complete the first requirement of the project proposal: including at least two actuators controlled via a closed-loop control system. One actuator was meant to raise and lower the robot arm while the second actuator was for turning the ultrasonic sensor to always face the ground. The actuator for the arm was successful, able to raise and lower the popsicle stick arm. The second actuator for rotating the ultrasonic sensor was working fine until a coding error caused both motors to rotate at 100% duty cycle and either damaged the gearbox or melted the insulation of the inside motor windings.
# 
#  Here is a video of the current state of the motor: <a href="https://youtu.be/8lpIKVaunlA">Motor State</a> 
# 
#  Before the motor was damaged, the sensor was able to rotate on the robotic arm. 
# 
#  Here is a video of the sensor rotating on the robotic arm: <a href="https://youtu.be/TMVUavhmCcw">Ultrasonic Arm Sensor Test</a> 
# 
#  However, this was the only modification to the project proposal. This project uses three sensors, one of which is a new sensor not covered in lab. The motor encoder and IMU sensors were used for the operation of the robot arm to keep track of the position of the motor and to input the desired position of the robotic arm respectively. The ultrasonic sensor is used to measure the distance between the top of the arm and the ground, where placing objects within this distance will give a rough estimate of the object’s height. This can be modified in the future to move the arm based on the desired position seen by the ultrasonic sensor, but the damaged second motor prevented an investigation to this portion of the project.
# 
#  Using these sensors and actuator, the project uses multitasking with Nucleo hardware running MicroPython to interact with the user in a non-trivial manner. The user interacts with the robot arm using the IMU to influence the angle of the robot arm, while the microcontroller pings the ultrasonic sensor every second for a distance measurement. While interfacing through the ultrasonic sensor’s UART ports would be the best way to get the distance measurement, I could not get that interface working for this project. If I had more time, I would improve this project by replacing the damaged motor for a working ultrasonic sensor system.
#
#  @section p_main Project FF Main Description
# 
# @image html figure1.jpg
# Figure 1. For the main code used in my final project, I first defined all imported modules, CPU pins, timer channels, and setup motor, encoder, IMU, and ultrasonic objects using the classes developed throughout the quarter.
#  
# @image html figure2.jpg
# Figure 2. Setup for the new ultrasonic sensor using the trigger and echo pins.
# 
# After these initializations, I set the values for certain variables used during the main loop of main
# 
# @image html figure3.jpg
# Figure 3. After these initializations, I set the values for certain variables used during the main loop of main.
# 
# In the main loop used during the project, I used multitasking principles to make sure that the ultrasonic sensor was not interrupting the closed loop control of the motors consistently, so I set functions to run every second for the update of the ultrasonic sensor (since that function incorporates delay for triggering)
# 
# @image html figure4.jpg
# Figure 4. Multitasking of ultrasonic sensor.
# 
# Next, the encoders and IMU angles are updated for motor position and euler angles respectively, where the vertical angle is converted into motor ticks for updating the motor.
# 
# @image html figure5.jpg
# Figure 5. Angular Position and Motor Position of IMU and motors respectively.
# 
# Next, the duty percent is found using the closed loop class and set by the motor class to each motor. The desired position is not reversed for both motors since they are facing toward each other, so the second motor will turn the opposite direction of the base motor.
# 
# @image html figure6.jpg
# Figure 6. Motor PWM duty cycle calculation and setting the motor wth the Kp found from the closed loop control class.
# 
# @image html figure7.jpg
# Figure 7. Printing of the distance sensed by the ultrasonic sensor, otherwise keeping a consistent 10 ms delay for closed loop control class.
#
#  @section sec_mot Motor Driver
#  The purpose of this driver is to control any Nucleo-Driver compatible motor.
#  The code used to create the object is: motor.MotorDriver(EN_pin, IN1_pin, IN2_pin, tim).
#  Testing motor driver included CCW and CW operation, as well as speed control using duty cycles.
#  Please see the motor class which is part of the \ref motor package.
#  <a href="https://bitbucket.org/scottjmangin/me405_labs/src/master/Lab1/">Repository Link</a>  
#
#  @section sec_enc Encoder Driver
#  The purpose of the encoder driver is to operate the encoder module on the Nucleo-Driver motors.
#  The code used to create the object is: encoder.EncoderDriver(pin_1, pin_2, timer). Testing
#  was done to verify no position overflow/underflow on absolute position, as well as getting position,
#  getting delta, and setting absolute position. Please see the encoder class which is part of the \ref encoder package.
#  <a href="https://bitbucket.org/scottjmangin/me405_labs/src/master/Lab2/">Repository Link</a>
#
#  @section sec_cloopkp Closed Loop Kp Controller
#  The purpose of the closed loop Kp controller is to operate the closed loop controller module on the Nucleo-Driver motors.
#  The code used to create the object is: closedloopKp.CLKpControl(). Testing was done to tune the value of Kp, verify that
#  the position of the motor was the same as the setpoint of motor ticks. Please see the closed loop control class which is part of the \ref closedloopKp package.
#  <a href="https://bitbucket.org/scottjmangin/me405_labs/src/master/Lab3/">Repository Link</a>
#
#  @section sec_IMUdriver IMU Driver
#  The purpose of the IMU driver is to operate the BNO055 sensor suite to use Euler angles and angular velocity to read the orientation of the BNO055 board.
#  The code used to create the object is: IMU.IMUDriver(I2C_ADDR). Testing was done to tune the value of Euler angles, verify that
#  the change in orientation of the BNO055 was the same as the change in orientation sensed by the sensor. Please see the IMU Driver class which is part of the \ref IMU package.
#  <a href="https://bitbucket.org/scottjmangin/me405_labs/src/master/Lab4/">Repository Link</a>
#
#  @section sec_ultra Ultrasonic Sensor Driver
#  The purpose of the Ultrasonic driver is to operate the URM37 sensor suite to use sound waves to read the distance of an object in front of the sensor.
#  The code used to create the object is: ultrasonic.USonic(echopin, trigpin). Testing was done to tune the distance value, verify that
#  the distance seen by the sensor was the same as the actual distance of the object from the sensor. Please see the ultrasonic driver class which is part of the \ref ultrasonic package.
#  <a href="https://bitbucket.org/scottjmangin/me405_labs/src/master/LabFF/">Repository Link</a>
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 22, 2020
#
#  @page sec_clkp Closed Loop (Kp Control)
#
#  When tuning my controller, I was looking for: the least amount of overshoot (under-damped respomse) while having the lowest time
#  constant so that the motor can get to the desired motor position fasts. The ideal motor response would be a Kp value that
#  makes a critically damped system, as it would reach the desired steady state value the quickest while having no overshoot.
#
#  @image html Kp1.JPG
#
#  Figure 1. This first tuning was good, but there was some overshoot with the position compared to the desired motor position. This Kp
#  value (-100) is not the entire value, as the error value of motor ticks is already divided by the total ticks per 16 bit (65535)
#  and multiplied by a duty cycle percentage (100). While this is a good result, I would like zero overshoot in my tuned controller.
#
#  @image html Kp2.JPG
#
#  Figure 2. This second tuning wasn't nearly as good, where the value of Kp (-10) was not enough to even reach the desired steady state
#  motor position value from the step input.
#
#  @image html Kp3.JPG
#
#  Figure 3. This final image shows the final tuned controller value of Kp (-45). As shown, the motor response is almost critically
#  damped and reaching the desired steady state value of the step input, reaching the steady state value as fast as possible
#  with no overshoot.
#
#  Here is a video of the final code working as intended:
#  <a href="https://youtu.be/xORRRIuPiw4 ">Video Link</a>
#
#  *Note, if there are no pictures available on this page, please visit: <a href="https://imgur.com/a/KSRUF9A">Figure Link</a>  
#
#  @page sec_IMU IMU Driver
#
#  Testing the IMU was important to make sure it was functioning correctly. First, I tested the output
#  from the BNO055 to make sure that it was changing based on the orientation of the sensor. This was important
#  in debugging that I wrote the wrong instructions to the board (wrong OP_MODE). Next, I looked in the datasheet
#  for the ratio of LSB ticks to degrees, and divided each important output (Euler angle, angular veloctiy) by its'
#  respective ratio (both had a ratio of 16 LSB to 1 degree/dps) to end with correct euler and angular velocity values
#  through the use of the ustruct package to divide each 6 byte total orientation segments into 3 signed cardinal directions
#  (x, y, z).
#
#  A video of the IMU in action can be found here:
#  <a href="https://youtu.be/avn7DRjAwaI ">Video Link</a>
#
#  @page sec_pFF Project FF Proposal
#
#  My project will aim to complete a 2-DOF robot arm that will be able to be manipulated by the IMU and be tracked by the motors' encoder
#  in a closed-loop feedback control. The purpose of the robot arm will be either: 1. To test the accuracy and sensitivity of the <a href="https://www.dfrobot.com/product-1302.html ">GPS</a>
#  module from DFRobot in terms of raising and lowering the arm and measuring accordingly, or 2. Test the accuracy and sensitivity of the 
#  <a href="https://www.dfrobot.com/product-53.html ">ultrasonic</a> sensor module by raising and lowering the robotic arm (and keeping the ultrasonic sensor facing the ground). It could
#  also have a function (with the ultrasonic sensor) to raise and lower depending on the distance specified by the user.
#
#  This project complies with the project requirements because it will use a closed-loop control system with two actuators and three sensors, one of which is a new sensor not found in
#  lab. It also interacts with the user in a non-trivial by interacting with the IMU to specify the orientation of the robot arm.
#
#  For this project, I will be needing popsicle sticks (for the actuating arms and ultrasonic or GPS platform), the GPS/ultrasonic sensor from DFRobot, as well as electrical tape to attach the sticks to the motor output shafts.
#  I only currently have electrical tape, but popsicle sticks are easy to obtain locally. The component with the most lead time will be the sensor itself, so ordering this from DFRobot as soon as possible will be imperative. 
#  A basic bill of materials would be: Popsicle sticks - 1 pack for $9.00 (Amazon) and the ultrasonic sensor - 1 for $13.90.
#
#  For manufacturing and assembly, I will be using tape to adhere the popsicle sticks to the motor as well as together if needed. The popsicle sticks will be cut to length using scissors or an exacto knife.
#  I have the tools I will need to manufacture this project.
#
#  Potential hazards for this project could be: potential pinch points during construction, rotating popsicle arms, potential for cuts from the exacto or scissors, and any potential for electricution from
#  improper wiring. However these hazards are not significant safety risks. To combat these risks, I will be using eye protection.
#
#  General Timeline for this project:
#
#  5/24 - Ordered DFRobot sensor and procure popsicle sticks
#
#  5/29 - Have basic robot arm set up with both motors active
#
#  6/1 - Have code for actuating robot arm with IMU, sensor arrival sometime this week
#
#  6/5 - Have working robot code (with IMU and ultrasonic sensor active)
#
#  6/10 - Have project demonstration and report ready for turn-in.
# 
#

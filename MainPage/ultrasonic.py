## @file ultrasonic.py
#  This module contains an ultrasonic class that is used in the ME 405 term project.
#
#  The ultrasonic class consists of several functions: init and update. Init initializes the inputted pins for creating ultrasonic
#  classes, and update sends a signal to the trigger pin and reads the corresponding distance sent through the echo pin, returning
#  the distance of the ultrasonic sensor in cm.
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date June 11, 2020
#
#  @package ultrasonic
#  This module contains an ultrasonic class that is used in the ME 405 term project. 
#
#  The ultrasonic class consists of several functions: init and update. Init initializes the inputted pins for creating ultrasonic
#  classes, and update sends a signal to the trigger pin and reads the corresponding distance sent through the echo pin, returning
#  the distance of the ultrasonic sensor in cm.
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date June 11, 2020

import pyb
import utime
import machine

## An ultrasonic driver object
#
#  The ultrasonic class consists of several functions: init and update. Init initializes the inputted pins for creating ultrasonic
#  classes, and update sends a signal to the trigger pin and reads the corresponding distance sent through the echo pin, returning
#  the distance of the ultrasonic sensor in cm.
#
#  @author Scott Mangin
#  @copyright 2020
#  @date June 11, 2020
class USonic:
    ''' This class implements an ultrasonic driver for the ME405 board.'''
    
    def __init__ (self, echopin, trigpin):
        ''' Initializes the ultrasonic sensor object'''
        ##
        # @param echopin A pyb.Pin input object that specifies the pin for the distance reading of the sensor.
        # @param trigpin A pyb,Pin output object that sends a desired length of trigger time to the sensor.
        
        # Set internal variables in case multiple ultrasonic sensors.
        self.pin_ECHO = echopin 
        self.pin_URTRIG = trigpin
        print('Initializing Ultrasonic')
    
    def update (self):
        ''' Sends a signal to the trigger pin for time, receives the echo signal from the sensor. '''
        
        # Confirms that trigger pin is off
        self.pin_URTRIG(0)
        utime.sleep_ms(5)
        
        # Toggles trigger pin high and low for triggering distance fetch
        self.pin_URTRIG(1)
        utime.sleep_ms(5)
        self.pin_URTRIG(0)
        
        # Reads the input from echo pin on low trigger for distance input
        duration = machine.time_pulse_us(self.pin_ECHO, 0)
        
        # Distance measurmeent based on manufacturer's ratio of input signal to distance (in cm)
        distance = duration / 50
        
        return distance
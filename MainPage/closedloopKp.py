## @file closedloopKp.py
#  This module contains a motor class that will be used in future ME 405 projects. 
#
#  This closed loop Kp control class consists of several functions: init and update. The update function takes the desired Kp
#  value and current motor position (as well as the desired motor position (in ticks) and returns a duty cycle value. This value
#  is saturated to either 100 or -100 (% duty cycle) as duty cycle cannot go higher than 100.
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 13, 2020
#
#  @package closedloopKp
#  This module contains a motor class that will be used in future ME 405 projects. 
#
#  This closed loop Kp control class consists of several functions: init and update. The update function takes the desired Kp
#  value and current motor position (as well as the desired motor position (in ticks) and returns a duty cycle value. This value
#  is saturated to either 100 or -100 (% duty cycle) as duty cycle cannot go higher than 100.
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date May 13, 2020

import pyb

## A Proportional Controller object
#
#  This closed loop Kp control class consists of several functions: init and update. The update function takes the desired Kp
#  value and current motor position (as well as the desired motor position (in ticks) and returns a duty cycle value. This value
#  is saturated to either 100 or -100 (% duty cycle) as duty cycle cannot go higher than 100.
#  @author Scott Mangin
#  @copyright 2020
#  @date May 13, 2020
class CLKpControl:
    ''' This class implements a closed loop proportional controller for the
    ME405 board.
    '''

    def __init__ (self):
        ''' Initializes the closed loop control system with Kp control.'''

    def update (self, SpecKp, motpos, setpoint):
        ''' Takes the desired motor position, setpoint, and Kp value and outputs a duty cycle percentage.'''
        ##
        #@param SpecKp An incoming variable to use as the desired user inputted Kp value.
        #@param motpos An incoming variable to use as the current motor position value.
        #@param setpoint An incoming variable to use as the desired motor position value.
        
        err = motpos-setpoint #Error between the current and desired motor position
        Kp = (err/65535)*(100)*-SpecKp #Kp is calculated with a ratio of error per tick count and and percent duty cycle
        if abs(Kp) > 100: #If the value of the duty cycle is above 100 (above 100 percent), it is limited to 100 to prevent issues
            if Kp > 0:
                Kp = 100
            elif Kp < 0:
                Kp = -100
        return Kp #Returns the duty cycle percent for motor driver
## @file main.py
#  Main program used for running robotic arm through multitasking between closed loop control of motor actuators and ultrasonic sensor distance measurements.
#
#  Main program used for running robotic arm through multitasking between closed loop control of motor actuators and ultrasonic sensor distance measurements.
#
#  @author Scott Mangin
#
#  @copyright 2020
#
#  @date June 11, 2020
#
#

# Import desired modules
import pyb
import time
import utime
import machine
from machine import Pin

# Create the pin objects used for interfacing with the first motor driver
pin_EN = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4);
pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5);

# Create the pin objects used for interfacing with the second motor driver
pin_ENB = pyb.Pin (pyb.Pin.cpu.C1, pyb.Pin.OUT_PP);
pin_IN1B = pyb.Pin (pyb.Pin.cpu.A0);
pin_IN2B = pyb.Pin (pyb.Pin.cpu.A1);

# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq = 20000);
timb = pyb.Timer(5, freq = 20000);

# Create a motor object passing in the pins and timer of each respective motor
moe = motor.MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
moeb = motor.MotorDriver(pin_ENB, pin_IN1B, pin_IN2B, timb)

# Create the pin objects used for interfacing with the first motor encoder driver
pin_PB6 = pyb.Pin (pyb.Pin.cpu.B6);
pin_PB7 = pyb.Pin (pyb.Pin.cpu.B7);
tim4 = pyb.Timer(4, period=65535, prescaler=0)

# Create the pin objects used for interfacing with the second motor encoder driver
pin_PC6 = pyb.Pin (pyb.Pin.cpu.C6);
pin_PC7 = pyb.Pin (pyb.Pin.cpu.C7);
tim8 = pyb.Timer(8, period=65535, prescaler=0)

# Create an encoder object passing in the pins and timer of each respective encoder
enc1 = encoder.EncoderDriver(pin_PB6, pin_PB7, tim4)
enc2 = encoder.EncoderDriver(pin_PC6, pin_PC7, tim8)

# Setup for IMU:
# Define constant representing device address of IMU
I2C_ADDR = 0x28

# Initialize object driver for IMU
imu = IMU.IMUdriver(I2C_ADDR) 

# Turns on the IMU
imu.mode(1)

# Changes OP_MODE to NDOF
imu.mode(13)

# Closed Loop
clcon1 = closedloopKp.CLKpControl()
clcon2 = closedloopKp.CLKpControl()

# Ultrasonic
pin_ECHO = pyb.Pin (pyb.Pin.cpu.H0, Pin.IN) #Echo PIN
pin_URTRIG = pyb.Pin (pyb.Pin.cpu.H1, pyb.Pin.OUT_PP); #Trigger Pin

uson = ultrasonic.USonic(pin_ECHO, pin_URTRIG)

# Set Desired Constants
enc1.set_position(0)  # Reset motor positions
enc2.set_position(0)
ticksdeg = 1440.6/360 # Ratio of ticks per degree according to professor
kp = 100              # Increased from lab 3 due to increased load requirements
moe.enable()          # Enable motors
moeb.enable()

run = 0               # Reset run counter
while 1 == 1:
    
    if run == 100: #For cooperative multitasking, updates the ultrasonic sensor roughly once every second
        dist = round(uson.update()) #Takes in distance measurement from ultrasonic, rounded for user
                                    #(only accurate to plus-minus 1 cm)
    
    # Read IMU Euler angles and update encoder positions at start
    eul = imu.euler()
    enc1.update()
    enc2.update()
    
    # Get Encoder positions
    pos1 = enc1.get_position()
    pos2 = enc2.get_position()
    
    # Convert x-axis euler angle (up and down) to motor ticks
    desiredpos = eul[1]*ticksdeg
    
    # Calculate duty cycle using desired position and current motor position,
    # same magnitude since motors sitting opposite directions.
    kpduty1 = clcon1.update(kp,pos1,desiredpos)
    kpduty2 = clcon2.update(kp,pos2,desiredpos)
    
    # Set the duty cycle of each motor accordingly
    moe.set_duty(kpduty1)
    moeb.set_duty(kpduty2)
    
    # Print distance measurement and reset run counter, otherwise delay for 10 ms, allows for ~1 second refresh
    # on ultrasonic sensor
    if run == 100:
        print('Distance is {:} cm'.format(dist))
        run = 0
    else:
        utime.sleep_ms(10)
    
    # Increase run counter
    run = run + 1